const mongoose = require('mongoose')

mongoose.Promise = global.Promise
mongoose.connect("mongodb://localhost:27017/sandbox", {useMongoClient: true})

const connection = mongoose.connection

connection.on('close', () => {
    console.log('MongoDB connection closed')
    process.exit(0)
})

module.exports = mongoose
